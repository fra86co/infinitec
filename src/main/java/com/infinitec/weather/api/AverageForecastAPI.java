package com.infinitec.weather.api;

import com.infinitec.weather.domain.usecase.RetrieveAverageForecastUseCase;
import com.infinitec.weather.domain.model.AverageForecasts;
import io.swagger.annotations.Api;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
@Api
public class AverageForecastAPI
{
  private RetrieveAverageForecastUseCase retrieveAverageForecastUseCase;

  public AverageForecastAPI(RetrieveAverageForecastUseCase retrieveAverageForecastUseCase)
  {
    this.retrieveAverageForecastUseCase = retrieveAverageForecastUseCase;
  }

  @RequestMapping(value = "v1/cache/clear", method = GET)
  @CacheEvict(cacheNames = "ForecastRepositoryCache")
  public void clear()
  {
  }

  @RequestMapping(value = "v1/data/{city}", method = GET)
  public ResponseEntity forecast(@PathVariable String city)
  {
    Optional<AverageForecasts> forecasts = retrieveAverageForecastUseCase.execute(city);

    return forecasts.<ResponseEntity>map(ResponseEntity::ok)
      .orElseGet(() -> ResponseEntity.notFound().build());

  }

}
