package com.infinitec.weather.adapter_openweatherapi;

import com.infinitec.weather.domain.model.Forecast;
import com.infinitec.weather.domain.model.Forecasts;

import java.util.List;
import java.util.stream.Collectors;

public class OpenWeatherToDomainConverter
{
  public Forecasts convert(OpenWeatherForecasts openWeatherForecasts)
  {
    final List<Forecast> forecastsList = openWeatherForecasts.list.stream().map(this::convert)
                                                                  .collect(Collectors.toList());
    return new Forecasts(forecastsList);
  }

  private Forecast convert(OpenWeatherForecast f)
  {
    return new Forecast(f.dt, f.main.temp, f.main.pressure);
  }
}
