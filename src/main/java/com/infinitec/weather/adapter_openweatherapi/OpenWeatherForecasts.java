package com.infinitec.weather.adapter_openweatherapi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OpenWeatherForecasts
{
  public List<OpenWeatherForecast> list;
}
