package com.infinitec.weather.adapter_openweatherapi;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.time.ZonedDateTime;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OpenWeatherForecast
{
  @JsonFormat(shape = JsonFormat.Shape.NUMBER, timezone = "UTC")
  public ZonedDateTime dt;
  public ForecastData main;

}
