package com.infinitec.weather.adapter_openweatherapi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.math.BigDecimal;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ForecastData
{
  public BigDecimal temp;
  public BigDecimal pressure;

}
