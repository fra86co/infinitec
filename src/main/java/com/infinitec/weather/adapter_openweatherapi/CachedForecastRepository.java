package com.infinitec.weather.adapter_openweatherapi;

import com.infinitec.weather.domain.ForecastRepository;
import com.infinitec.weather.domain.model.Forecasts;

import org.springframework.cache.annotation.Cacheable;

import java.util.Optional;

public class CachedForecastRepository implements ForecastRepository
{
  private final ForecastRepository delegate;

  public CachedForecastRepository(ForecastRepository delegate)
  {
    this.delegate = delegate;
  }

  @Override
  @Cacheable(cacheNames = "ForecastRepositoryCache")
  public Optional<Forecasts> find(String cityName)
  {
    return delegate.find(cityName);
  }
}
