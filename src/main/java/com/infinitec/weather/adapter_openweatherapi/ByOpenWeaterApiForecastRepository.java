package com.infinitec.weather.adapter_openweatherapi;

import com.infinitec.weather.domain.ForecastRepository;
import com.infinitec.weather.domain.model.Forecasts;

import java.util.Optional;

public class ByOpenWeaterApiForecastRepository implements ForecastRepository
{

  private final OpenWeatherApiClient client;
  private final OpenWeatherToDomainConverter openWeatherToDomain;

  public ByOpenWeaterApiForecastRepository(OpenWeatherApiClient client,
    OpenWeatherToDomainConverter openWeatherToDomain)
  {
    this.client = client;
    this.openWeatherToDomain = openWeatherToDomain;
  }

  @Override
  public Optional<Forecasts> find(String cityName)
  {
    final Optional<OpenWeatherForecasts> openWeatherForecasts = client.find(cityName);

    return openWeatherForecasts.map(openWeatherToDomain::convert);
  }
}
