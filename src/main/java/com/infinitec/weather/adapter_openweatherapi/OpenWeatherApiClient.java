package com.infinitec.weather.adapter_openweatherapi;

import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;

public class OpenWeatherApiClient
{
  private final RestTemplate restTemplate;
  private String apiBaseUrl;
  private String appId;

  public OpenWeatherApiClient(RestTemplate restTemplate, String apiBaseUrl, String appId)
  {
    this.restTemplate = restTemplate;
    this.apiBaseUrl = apiBaseUrl;
    this.appId = appId;
  }

  public Optional<OpenWeatherForecasts> find(String cityName)
  {
    try
    {
      final OpenWeatherForecasts openWeatherForecasts = restTemplate
        .getForObject(apiBaseUrl + "/data/2.5/forecast?units=metric&q=" + cityName + "&appid=" + appId,
          OpenWeatherForecasts.class);

      return Optional.ofNullable(openWeatherForecasts);
    }
    catch (HttpClientErrorException e)
    {
      if (e.getStatusCode() == HttpStatus.NOT_FOUND)
      {
        return Optional.empty();
      }
      throw e;
    }

  }
}

