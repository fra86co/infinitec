package com.infinitec.weather.domain.model;

import com.infinitec.weather.domain.model.Forecast;

import java.util.List;

public class Forecasts
{
  public final List<Forecast> forecasts;

  public Forecasts(List<Forecast> forecasts)
  {
    this.forecasts = forecasts;
  }
}
