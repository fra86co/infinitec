package com.infinitec.weather.domain.model;

import java.util.List;

public class AverageForecasts
{
  public final List<AverageForecast> averageForecasts;

  public AverageForecasts(List<AverageForecast> averageForecasts)
  {
    this.averageForecasts = averageForecasts;
  }
}
