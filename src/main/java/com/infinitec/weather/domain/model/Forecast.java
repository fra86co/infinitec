package com.infinitec.weather.domain.model;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

public class Forecast
{
  public final ZonedDateTime dateTime;
  public final BigDecimal temperature;
  public final BigDecimal pressure;

  public Forecast(ZonedDateTime dateTime, BigDecimal temperature, BigDecimal pressure)
  {
    this.dateTime = dateTime;
    this.temperature = temperature;
    this.pressure = pressure;
  }

  @Override
  public String toString()
  {
    final StringBuilder sb = new StringBuilder("Forecast{");
    sb.append("dateTime=").append(dateTime);
    sb.append(", temperature=").append(temperature);
    sb.append(", pressure=").append(pressure);
    sb.append('}');
    return sb.toString();
  }
}
