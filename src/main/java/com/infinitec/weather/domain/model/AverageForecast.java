package com.infinitec.weather.domain.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

public class AverageForecast
{
  @JsonFormat(shape = JsonFormat.Shape.STRING)
  public final ZonedDateTime day;
  public final BigDecimal dailyAverage;
  public final BigDecimal nightlyAverage;
  public final BigDecimal pressureAverage;

  public AverageForecast(ZonedDateTime day, BigDecimal dailyAverage, BigDecimal nightlyAverage,
    BigDecimal pressureAverage)
  {
    this.day = day;
    this.dailyAverage = dailyAverage;
    this.nightlyAverage = nightlyAverage;
    this.pressureAverage = pressureAverage;
  }
}
