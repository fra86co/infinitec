package com.infinitec.weather.domain.rangefilter;

import com.infinitec.weather.domain.model.Forecasts;

import java.time.ZonedDateTime;

public interface ForecastsRangeFilter
{
  Forecasts filter(ZonedDateTime day, Forecasts forecasts);
}
