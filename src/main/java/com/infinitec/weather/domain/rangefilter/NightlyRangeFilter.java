package com.infinitec.weather.domain.rangefilter;

import com.infinitec.weather.domain.model.Forecast;
import com.infinitec.weather.domain.model.Forecasts;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.stream.Collectors;

public class NightlyRangeFilter implements ForecastsRangeFilter
{
  @Override
  public Forecasts filter(ZonedDateTime day, Forecasts forecasts)
  {
    final List<Forecast> filteredForecast = forecasts.forecasts.stream()
                                                               .filter(forecast -> filter(day, forecast))
                                                               .collect(Collectors.toList());

    return new Forecasts(filteredForecast);
  }

  private boolean filter(ZonedDateTime day, Forecast forecast)
  {
    return forecast.dateTime.isAfter(day.plusHours(18).minusMinutes(1))
      && forecast.dateTime.isBefore(day.plusHours(30).plusMinutes(1));
  }
}
