package com.infinitec.weather.domain;

import java.time.ZoneId;
import java.time.ZonedDateTime;

public class SytemClock implements Clock
{
  @Override
  public ZonedDateTime clock()
  {
    return ZonedDateTime.now(ZoneId.of("UTC"));
  }
}
