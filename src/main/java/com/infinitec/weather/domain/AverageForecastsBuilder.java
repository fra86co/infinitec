package com.infinitec.weather.domain;

import com.infinitec.weather.domain.model.AverageForecast;
import com.infinitec.weather.domain.model.AverageForecasts;
import com.infinitec.weather.domain.model.Forecasts;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.LinkedList;
import java.util.List;

public class AverageForecastsBuilder
{

  private final ForecastDayFilter forecastDayFilter;

  private final AverageCalculator pressureDayAverageCalculator;
  private final AverageCalculator dailyDayAverageCalculator;
  private final AverageCalculator nightlyDayAverageCalculator;

  public AverageForecastsBuilder(ForecastDayFilter forecastDayFilter,
    AverageCalculator pressureDayAverageCalculator,
    AverageCalculator dailyDayAverageCalculator,
    AverageCalculator nightlyDayAverageCalculator)
  {
    this.forecastDayFilter = forecastDayFilter;
    this.pressureDayAverageCalculator = pressureDayAverageCalculator;
    this.dailyDayAverageCalculator = dailyDayAverageCalculator;
    this.nightlyDayAverageCalculator = nightlyDayAverageCalculator;
  }

  public AverageForecasts build(Forecasts forecasts)
  {
    final LinkedList<AverageForecast> averageForecasts = new LinkedList<>();
    List<ZonedDateTime> days = forecastDayFilter.filter();

    for (ZonedDateTime day : days)
    {
      BigDecimal pressureDayAverage = pressureDayAverageCalculator.calculate(day, forecasts);
      BigDecimal dailyDayAverage = dailyDayAverageCalculator.calculate(day, forecasts);
      BigDecimal nightlyDayAverage = nightlyDayAverageCalculator.calculate(day, forecasts);

      AverageForecast averageForecast = new AverageForecast(day, dailyDayAverage, nightlyDayAverage,
        pressureDayAverage);
      averageForecasts.add(averageForecast);
    }

    return new AverageForecasts(averageForecasts);
  }

}
