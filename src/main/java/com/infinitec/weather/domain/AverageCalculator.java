package com.infinitec.weather.domain;

import com.infinitec.weather.domain.model.Forecasts;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

public interface AverageCalculator
{
  public BigDecimal calculate(ZonedDateTime day, Forecasts forecasts);
}
