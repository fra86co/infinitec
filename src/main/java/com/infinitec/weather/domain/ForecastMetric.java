package com.infinitec.weather.domain;

import com.infinitec.weather.domain.model.Forecasts;

import java.math.BigDecimal;

public interface ForecastMetric
{
  BigDecimal apply(Forecasts forecasts);
}
