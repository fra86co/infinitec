package com.infinitec.weather.domain;

import java.time.ZonedDateTime;

public interface Clock
{
  ZonedDateTime clock();
}
