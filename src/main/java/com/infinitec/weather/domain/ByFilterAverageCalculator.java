package com.infinitec.weather.domain;

import com.infinitec.weather.domain.model.Forecasts;
import com.infinitec.weather.domain.rangefilter.ForecastsRangeFilter;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

public class ByFilterAverageCalculator implements AverageCalculator
{
  private final ForecastsRangeFilter forecastsRangeFilter;
  private final ForecastMetric forecastMetric;

  public ByFilterAverageCalculator(ForecastsRangeFilter forecastsRangeFilter,
    ForecastMetric forecastMetric)
  {
    this.forecastsRangeFilter = forecastsRangeFilter;
    this.forecastMetric = forecastMetric;
  }

  @Override
  public BigDecimal calculate(ZonedDateTime day, Forecasts forecasts)
  {
    Forecasts filteredForecasts = forecastsRangeFilter.filter(day, forecasts);

    return forecastMetric.apply(filteredForecasts);
  }
}
