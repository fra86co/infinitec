package com.infinitec.weather.domain;

import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static java.util.Arrays.asList;

public class ForecastDayFilter
{
  private final Clock clock;

  public ForecastDayFilter(Clock clock)
  {
    this.clock = clock;
  }

  public List<ZonedDateTime> filter()
  {
    final ZonedDateTime clock = this.clock.clock();
    final ZonedDateTime truncatedDay = clock.truncatedTo(ChronoUnit.DAYS);
    return asList(truncatedDay.plusDays(1), truncatedDay.plusDays(2), truncatedDay.plusDays(3));
  }
}
