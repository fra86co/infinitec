package com.infinitec.weather.domain;

import com.infinitec.weather.domain.model.Forecast;
import com.infinitec.weather.domain.model.Forecasts;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.function.Function;

public class ByFunctionForecastMetric implements ForecastMetric
{
  private final Function<Forecast, BigDecimal> forecastBigDecimalFunction;

  public ByFunctionForecastMetric(Function<Forecast, BigDecimal> forecastBigDecimalFunction)
  {
    this.forecastBigDecimalFunction = forecastBigDecimalFunction;
  }

  @Override
  public BigDecimal apply(Forecasts forecasts)
  {
    final BigDecimal sum = forecasts.forecasts.stream().map(forecastBigDecimalFunction)
                                              .reduce(BigDecimal.ZERO, BigDecimal::add);

    return sum.setScale(2, BigDecimal.ROUND_HALF_UP)
              .divide(new BigDecimal(forecasts.forecasts.size()), RoundingMode.HALF_UP);
  }

}
