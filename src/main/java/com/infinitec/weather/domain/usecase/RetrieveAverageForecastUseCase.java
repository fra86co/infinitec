package com.infinitec.weather.domain.usecase;

import com.infinitec.weather.domain.model.AverageForecasts;

import java.util.Optional;

public interface RetrieveAverageForecastUseCase
{
  Optional<AverageForecasts> execute(String city);
}
