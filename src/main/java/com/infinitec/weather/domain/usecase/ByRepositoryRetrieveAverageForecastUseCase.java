package com.infinitec.weather.domain.usecase;

import com.infinitec.weather.domain.AverageForecastsBuilder;
import com.infinitec.weather.domain.ForecastRepository;
import com.infinitec.weather.domain.model.AverageForecasts;
import com.infinitec.weather.domain.model.Forecasts;

import java.util.Optional;

public class ByRepositoryRetrieveAverageForecastUseCase implements RetrieveAverageForecastUseCase
{
  private ForecastRepository forecastRepository;
  private final AverageForecastsBuilder averageForecastsBuilder;

  public ByRepositoryRetrieveAverageForecastUseCase(ForecastRepository forecastRepository,
    AverageForecastsBuilder averageForecastsBuilder)
  {
    this.forecastRepository = forecastRepository;
    this.averageForecastsBuilder = averageForecastsBuilder;
  }

  @Override
  public Optional<AverageForecasts> execute(String cityName)
  {
    final Optional<Forecasts> forecasts = forecastRepository.find(cityName);

    return forecasts.map(averageForecastsBuilder::build);
  }
}
