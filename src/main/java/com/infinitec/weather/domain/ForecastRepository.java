package com.infinitec.weather.domain;

import com.infinitec.weather.domain.model.Forecasts;

import java.util.Optional;

public interface ForecastRepository
{
  Optional<Forecasts> find(String cityName);
}
