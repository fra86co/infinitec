package com.infinitec.weather.main.swagger;

import io.swagger.annotations.Api;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableSwagger2
public class SwaggerConfig
{
  private static final String TITLE = "Weather Forecast Average Api";
  private static final String DESCRIPTION = "";
  private static final Contact CONTACT = new Contact("", "", "");

  @Bean
  public Docket createConfiguration()
  {
    return new Docket(DocumentationType.SWAGGER_2)
      .select()
      .apis(RequestHandlerSelectors.withClassAnnotation(Api.class))
      .paths(PathSelectors.any())
      .build()
      .apiInfo(apinfo())
      .pathMapping("/");
  }

  private ApiInfo apinfo()
  {
    return new ApiInfo(TITLE, DESCRIPTION, getApplicationVersion(), null, CONTACT, null,
      null);
  }

  public final static String getApplicationVersion()
  {
    Package sourcePackage = SwaggerConfig.class.getPackage();
    return (sourcePackage == null ? null : sourcePackage.getImplementationVersion());
  }
}
