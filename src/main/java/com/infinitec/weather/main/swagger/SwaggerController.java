package com.infinitec.weather.main.swagger;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class SwaggerController
{
  @RequestMapping("/docs")
  public String doGet()
  {
    return "redirect:/swagger-ui.html";
  }
}
