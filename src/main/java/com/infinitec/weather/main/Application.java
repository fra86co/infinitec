package com.infinitec.weather.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"com.infinitec.weather.main", "com.infinitec.weather.api"})
public class Application
{

  public static void main(String[] args)
  {
    SpringApplication.run(Application.class, args);
  }

}
