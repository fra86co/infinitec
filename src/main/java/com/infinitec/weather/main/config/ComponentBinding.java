package com.infinitec.weather.main.config;

import com.infinitec.weather.adapter_openweatherapi.ByOpenWeaterApiForecastRepository;
import com.infinitec.weather.adapter_openweatherapi.OpenWeatherApiClient;
import com.infinitec.weather.adapter_openweatherapi.OpenWeatherToDomainConverter;
import com.infinitec.weather.domain.AverageForecastsBuilder;
import com.infinitec.weather.domain.ByFilterAverageCalculator;
import com.infinitec.weather.domain.ByFunctionForecastMetric;
import com.infinitec.weather.domain.usecase.ByRepositoryRetrieveAverageForecastUseCase;
import com.infinitec.weather.domain.ForecastDayFilter;
import com.infinitec.weather.domain.ForecastRepository;
import com.infinitec.weather.domain.usecase.RetrieveAverageForecastUseCase;
import com.infinitec.weather.domain.SytemClock;
import com.infinitec.weather.domain.rangefilter.DailyRangeFilter;
import com.infinitec.weather.domain.rangefilter.NightlyRangeFilter;
import com.infinitec.weather.domain.rangefilter.PressureRangeFilter;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class ComponentBinding
{

  @Value("${openweahter.api.appid}")
  private String appId;
  @Value("${openweahter.api.url}")
  private String apiBaseUrl;

  @Bean
  public RetrieveAverageForecastUseCase retrieveAverageForecastUseCase(ForecastRepository forecastRepository)
  {

    return new ByRepositoryRetrieveAverageForecastUseCase(forecastRepository, averageForecastsBuilder());
  }

  @Bean
  public ForecastRepository forecastRepository()
  {
    return new ByOpenWeaterApiForecastRepository(client(), new OpenWeatherToDomainConverter());
  }

  private AverageForecastsBuilder averageForecastsBuilder()
  {
    return new AverageForecastsBuilder(
      new ForecastDayFilter(new SytemClock()),
      new ByFilterAverageCalculator(new PressureRangeFilter(),
        new ByFunctionForecastMetric(forecast -> forecast.pressure)),
      new ByFilterAverageCalculator(new DailyRangeFilter(),
        new ByFunctionForecastMetric(forecast -> forecast.temperature)),
      new ByFilterAverageCalculator(new NightlyRangeFilter(),
        new ByFunctionForecastMetric(forecast -> forecast.temperature))
    );
  }

  private OpenWeatherApiClient client()
  {
    final RestTemplate restTemplate = getRestTemplate();
    return new OpenWeatherApiClient(restTemplate, apiBaseUrl, appId);
  }

  private RestTemplate getRestTemplate()
  {
    return new RestTemplateBuilder().build();
  }
}
