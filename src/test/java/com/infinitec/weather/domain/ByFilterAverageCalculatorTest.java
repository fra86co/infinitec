package com.infinitec.weather.domain;

import com.infinitec.weather.domain.model.Forecasts;
import com.infinitec.weather.domain.rangefilter.ForecastsRangeFilter;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;

import static org.hamcrest.Matchers.comparesEqualTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

public class ByFilterAverageCalculatorTest
{

  private ForecastsRangeFilter forecastsRangeFilter;
  private ForecastMetric forecastMetric;

  @Before
  public void setUp() throws Exception
  {
    forecastsRangeFilter = Mockito.mock(ForecastsRangeFilter.class);
    forecastMetric = Mockito.mock(ForecastMetric.class);
  }

  @Test
  public void average()
  {
    final ZonedDateTime day = aDate();
    final Forecasts forecasts = new Forecasts(new LinkedList<>());
    final Forecasts filteredForecasts = new Forecasts(new LinkedList<>());
    final BigDecimal calculatedAverage = new BigDecimal("1");

    when(forecastsRangeFilter.filter(day, forecasts)).thenReturn(filteredForecasts);
    when(forecastMetric.apply(filteredForecasts)).thenReturn(calculatedAverage);

    ByFilterAverageCalculator underTest = new ByFilterAverageCalculator(forecastsRangeFilter, forecastMetric);

    final BigDecimal average = underTest.calculate(day, forecasts);

    assertThat(average, comparesEqualTo(calculatedAverage));

  }

  private ZonedDateTime aDate()
  {
    return ZonedDateTime
      .parse("10-10-2010 00:00", DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm").withZone(ZoneId.of("UTC")));
  }
}