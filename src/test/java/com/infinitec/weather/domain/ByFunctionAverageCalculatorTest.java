package com.infinitec.weather.domain;

import com.infinitec.weather.domain.model.Forecast;
import com.infinitec.weather.domain.model.Forecasts;
import org.hamcrest.Matchers;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.LinkedList;

import static org.junit.Assert.assertThat;

public class ByFunctionAverageCalculatorTest
{

  @Test
  public void averageOnPressure()
  {
    final LinkedList<Forecast> list = new LinkedList<>();
    list.add(new Forecast(null, null, new BigDecimal("5")));
    list.add(new Forecast(null, null, new BigDecimal("1")));
    list.add(new Forecast(null, null, new BigDecimal("3")));
    list.add(new Forecast(null, null, new BigDecimal("6")));
    final Forecasts forecasts = new Forecasts(list);

    ByFunctionForecastMetric underTest = new ByFunctionForecastMetric(f -> f.pressure);

    final BigDecimal average = underTest.apply(forecasts);
    assertThat(average, Matchers.comparesEqualTo(new BigDecimal("3.75")));
  }
}