package com.infinitec.weather.domain;

import org.hamcrest.Matchers;
import org.junit.Test;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class ForecastDayFilterTest
{

  @Test
  public void nextThreeDays()
  {

    ForecastDayFilter underTest = new ForecastDayFilter(new StubbedClock(dateFor("11-12-2012 10:00")));

    final List<ZonedDateTime> times = underTest.filter();

    assertThat(times, Matchers.hasSize(3));
    assertThat(times.get(0), is(dateFor("12-12-2012 00:00")));
    assertThat(times.get(1), is(dateFor("13-12-2012 00:00")));
    assertThat(times.get(2), is(dateFor("14-12-2012 00:00")));

  }

  private ZonedDateTime dateFor(String s)
  {
    return ZonedDateTime.parse(s, DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm").withZone(ZoneId.of("UTC")));
  }

  private class StubbedClock implements Clock
  {
    private final ZonedDateTime zonedDateTime;

    public StubbedClock(ZonedDateTime zonedDateTime)
    {
      this.zonedDateTime = zonedDateTime;
    }

    public ZonedDateTime clock()
    {
      return zonedDateTime;
    }
  }
}