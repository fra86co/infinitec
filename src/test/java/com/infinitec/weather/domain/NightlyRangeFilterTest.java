package com.infinitec.weather.domain;

import com.infinitec.weather.domain.model.Forecast;
import com.infinitec.weather.domain.model.Forecasts;
import com.infinitec.weather.domain.rangefilter.NightlyRangeFilter;
import org.hamcrest.Matchers;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;

import static org.hamcrest.Matchers.hasItem;
import static org.junit.Assert.assertThat;

public class NightlyRangeFilterTest
{

  @Test
  public void nightlyAverage()
  {
    final LinkedList<Forecast> list = new LinkedList<>();
    final Forecast validForecast = new Forecast(dateFor("10-10-2010 18:00"), BigDecimal.ONE, new BigDecimal("1"));
    final Forecast validForecast2 = new Forecast(dateFor("10-10-2010 21:00"), BigDecimal.ONE, new BigDecimal("1"));
    final Forecast validForecast3 = new Forecast(dateFor("11-10-2010 06:00"), BigDecimal.ONE, new BigDecimal("3"));

    list.add(new Forecast(dateFor("09-10-2010 03:00"), BigDecimal.ONE, new BigDecimal("1110.36")));
    list.add(new Forecast(dateFor("10-10-2010 15:00"), BigDecimal.ONE, new BigDecimal("1110.36")));
    list.add(validForecast);
    list.add(validForecast2);
    list.add(validForecast3);
    list.add(new Forecast(dateFor("11-10-2010 09:00"), BigDecimal.ONE, new BigDecimal("369.52")));
    final Forecasts forecasts = new Forecasts(list);

    NightlyRangeFilter underTest = new NightlyRangeFilter();

    final Forecasts filtered = underTest.filter(dateFor("10-10-2010 00:00"), forecasts);

    assertThat(filtered.forecasts, Matchers.hasSize(3));
    assertThat(filtered.forecasts, hasItem(validForecast));
    assertThat(filtered.forecasts, hasItem(validForecast2));
    assertThat(filtered.forecasts, hasItem(validForecast3));

  }

  private ZonedDateTime dateFor(String s)
  {
    return ZonedDateTime.parse(s, DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm").withZone(ZoneId.of("UTC")));
  }
}