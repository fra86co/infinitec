package com.infinitec.weather.domain;

import com.infinitec.weather.domain.model.AverageForecasts;
import com.infinitec.weather.domain.model.Forecasts;
import com.infinitec.weather.domain.usecase.ByRepositoryRetrieveAverageForecastUseCase;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Optional;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

public class ByRepositoryRetrieveAverageForecastUseCaseTest
{
  private ForecastRepository forecastRepository;
  private AverageForecastsBuilder averageForecastsBuilder;
  private ByRepositoryRetrieveAverageForecastUseCase underTest;

  @Before
  public void setUp() throws Exception
  {
    forecastRepository = Mockito.mock(ForecastRepository.class);
    averageForecastsBuilder = Mockito.mock(AverageForecastsBuilder.class);
    underTest = new ByRepositoryRetrieveAverageForecastUseCase(forecastRepository, averageForecastsBuilder);
  }

  @Test
  public void forecastAverageForTomorrow() throws Exception
  {
    AverageForecasts averageForecasts = new AverageForecasts(Arrays.asList());

    final Forecasts forecasts = new Forecasts(new LinkedList<>());
    when(forecastRepository.find("milano")).thenReturn(Optional.of(forecasts));
    when(averageForecastsBuilder.build(forecasts)).thenReturn(averageForecasts);

    final Optional<AverageForecasts> forecastsResponse = underTest.execute("milano");

    assertThat(forecastsResponse.get(), is(averageForecasts));
  }

}