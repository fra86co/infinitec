package com.infinitec.weather.adapter_openweatherapi.builder;

import com.infinitec.weather.adapter_openweatherapi.ForecastData;
import com.infinitec.weather.adapter_openweatherapi.OpenWeatherForecast;

import java.time.ZonedDateTime;

public final class OpenWeatherForecastBuilder
{
  private ZonedDateTime dt = ZonedDateTime.now();
  private ForecastData main = ForecastDataBuilder.aForecastData().build();

  private OpenWeatherForecastBuilder()
  {
  }

  public static OpenWeatherForecastBuilder anOpenWeatherForecast()
  {
    return new OpenWeatherForecastBuilder();
  }

  public OpenWeatherForecastBuilder withDt(ZonedDateTime val)
  {
    dt = val;
    return this;
  }

  public OpenWeatherForecastBuilder withMain(ForecastData val)
  {
    main = val;
    return this;
  }

  public OpenWeatherForecast build()
  {
    OpenWeatherForecast o = new OpenWeatherForecast();
    o.dt = dt;
    o.main = main;
    return o;
  }
}
