package com.infinitec.weather.adapter_openweatherapi.builder;

import com.infinitec.weather.adapter_openweatherapi.ForecastData;

import java.math.BigDecimal;

public final class ForecastDataBuilder
{
  private BigDecimal temp = new BigDecimal("10.10");
  private BigDecimal pressure = new BigDecimal("100.10");

  private ForecastDataBuilder()
  {
  }

  public static ForecastDataBuilder aForecastData()
  {
    return new ForecastDataBuilder();
  }

  public ForecastDataBuilder withTemp(BigDecimal val)
  {
    temp = val;
    return this;
  }

  public ForecastDataBuilder withPressure(BigDecimal val)
  {
    pressure = val;
    return this;
  }

  public ForecastData build()
  {
    ForecastData f = new ForecastData();
    f.temp = temp;
    f.pressure = pressure;
    return f;
  }
}
