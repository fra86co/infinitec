package com.infinitec.weather.adapter_openweatherapi;

import org.junit.Before;
import org.junit.Test;

import org.springframework.http.MediaType;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

import static java.nio.file.Files.readAllBytes;
import static java.nio.file.Paths.get;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

public class OpenWeatherApiClientTest
{

  private OpenWeatherApiClient underTest;
  private MockRestServiceServer mockServer;

  @Before
  public void setUp() throws Exception
  {
    RestTemplate restTemplate = new RestTemplate();
    mockServer = MockRestServiceServer.bindTo(restTemplate).build();
    underTest = new OpenWeatherApiClient(
      restTemplate,
      "http://api.base.url",
      "an_app_id");
  }

  @Test
  public void forcast() throws Exception
  {
    mockServer
      .expect(
        requestTo("http://api.base.url/data/2.5/forecast?units=metric&q=milano&appid=an_app_id"))
      .andRespond(withSuccess(jsonFrom("/milan_forecast.json"), MediaType.APPLICATION_JSON));

    final Optional<OpenWeatherForecasts> response = underTest.find("milano");

    OpenWeatherForecasts forecasts = response.get();
    assertThat(forecasts.list, hasSize(40));
    assertThat(forecasts.list.get(0).dt, is(dateFor("2018-10-07T21:00:00+00:00[UTC]")));
    assertThat(forecasts.list.get(0).main.temp, is(new BigDecimal("285.79")));
    assertThat(forecasts.list.get(0).main.pressure, is(new BigDecimal("1004.18")));

    assertThat(forecasts.list.get(1).dt, is(dateFor("2018-10-08T00:00:00+00:00[UTC]")));
    assertThat(forecasts.list.get(1).main.temp, is(new BigDecimal("284.61")));
    assertThat(forecasts.list.get(1).main.pressure, is(new BigDecimal("1004.78")));
  }

  private ZonedDateTime dateFor(String s) throws ParseException
  {
    return ZonedDateTime.parse(s, DateTimeFormatter.ISO_DATE_TIME);
  }

  private String jsonFrom(String file) throws URISyntaxException, IOException
  {
    return new String(readAllBytes(get(OpenWeatherApiClientTest.class
      .getResource(file).toURI())), "UTF-8");
  }
}