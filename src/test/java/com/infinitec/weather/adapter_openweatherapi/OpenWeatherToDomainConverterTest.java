package com.infinitec.weather.adapter_openweatherapi;

import com.infinitec.weather.adapter_openweatherapi.builder.ForecastDataBuilder;
import com.infinitec.weather.domain.model.Forecasts;
import org.hamcrest.Matchers;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;

import static com.infinitec.weather.adapter_openweatherapi.builder.OpenWeatherForecastBuilder.anOpenWeatherForecast;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class OpenWeatherToDomainConverterTest
{

  @Test
  public void convert()
  {
    final OpenWeatherForecasts openWeatherForecasts = new OpenWeatherForecasts();
    openWeatherForecasts.list = new LinkedList<>();
    openWeatherForecasts.list.add(
      anOpenWeatherForecast()
        .withDt(dateFor("10-10-2000 11:12"))
        .withMain(ForecastDataBuilder.aForecastData()
                                     .withPressure(new BigDecimal("11.10"))
                                     .withTemp(new BigDecimal("111.10"))
                                     .build())
        .build());
    openWeatherForecasts.list.add(
      anOpenWeatherForecast()
        .withDt(dateFor("11-10-2000 11:12"))
        .withMain(ForecastDataBuilder.aForecastData()
                                     .withPressure(new BigDecimal("21.10"))
                                     .withTemp(new BigDecimal("211.10"))
                                     .build())
        .build());

    OpenWeatherToDomainConverter underTest = new OpenWeatherToDomainConverter();

    final Forecasts convert = underTest.convert(openWeatherForecasts);

    assertThat(convert.forecasts, Matchers.hasSize(2));
    assertThat(convert.forecasts.get(0).dateTime, is(dateFor("10-10-2000 11:12")));
    assertThat(convert.forecasts.get(0).pressure, is(new BigDecimal("11.10")));
    assertThat(convert.forecasts.get(0).temperature, is(new BigDecimal("111.10")));
    assertThat(convert.forecasts.get(1).dateTime, is(dateFor("11-10-2000 11:12")));
    assertThat(convert.forecasts.get(1).pressure, is(new BigDecimal("21.10")));
    assertThat(convert.forecasts.get(1).temperature, is(new BigDecimal("211.10")));

  }

  private ZonedDateTime dateFor(String s)
  {
    return ZonedDateTime.parse(s, DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm").withZone(ZoneId.of("UTC")));
  }
}