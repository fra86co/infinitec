package com.infinitec.weather.api;

import com.infinitec.weather.domain.model.AverageForecast;
import com.infinitec.weather.domain.model.AverageForecasts;
import com.infinitec.weather.domain.usecase.RetrieveAverageForecastUseCase;

import java.math.BigDecimal;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Optional;

public class StubbedRetrieveAverageForecastUseCase implements RetrieveAverageForecastUseCase
{

  private final String city;

  public StubbedRetrieveAverageForecastUseCase(String city)
  {

    this.city = city;
  }

  @Override
  public Optional<AverageForecasts> execute(String city)
  {
    if (!this.city.equals(city))
    {
      return Optional.empty();
    }

    final AverageForecast[] averageForecasts = new AverageForecast[] {
      new AverageForecast(dateFor("12-12-2000 00:00"),
        new BigDecimal("10.33"),
        new BigDecimal("8.99"),
        new BigDecimal(30)),
      new AverageForecast(dateFor("13-12-2000 00:00"),
        new BigDecimal("11.33"),
        new BigDecimal("81.99"),
        new BigDecimal(90))};

    return Optional.of(
      new AverageForecasts(Arrays.asList(averageForecasts)
      ));
  }

  private ZonedDateTime dateFor(String s)
  {
    return ZonedDateTime.parse(s, DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm").withZone(ZoneId.of("UTC")));
  }

}
