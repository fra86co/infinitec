package com.infinitec.weather.api;

import org.junit.Before;
import org.junit.Test;

import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class AverageForecastAPITest
{

  private MockMvc mockMvc;

  @Before
  public void setUp() throws Exception
  {
    mockMvc = MockMvcBuilders
      .standaloneSetup(new AverageForecastAPI(new StubbedRetrieveAverageForecastUseCase("milano")))
      .build();
  }

  @Test
  public void shouldProvideForecastForACity() throws Exception
  {
    mockMvc.perform(
      get("/v1/data/milano"))
           .andExpect(status().is(200))
           .andExpect(jsonPath("averageForecasts", hasSize(2)))
           .andExpect(jsonPath("averageForecasts[0].day", is("2000-12-12T00:00:00Z")))
           .andExpect(jsonPath("averageForecasts[0].dailyAverage", is(10.33)))
           .andExpect(jsonPath("averageForecasts[0].nightlyAverage", is(8.99)))
           .andExpect(jsonPath("averageForecasts[0].pressureAverage", is(30)))
           .andExpect(jsonPath("averageForecasts[1].day", is("2000-12-13T00:00:00Z")))
           .andExpect(jsonPath("averageForecasts[1].dailyAverage", is(11.33)))
           .andExpect(jsonPath("averageForecasts[1].nightlyAverage", is(81.99)))
           .andExpect(jsonPath("averageForecasts[1].pressureAverage", is(90)))
    ;

  }


  @Test
  public void notFound() throws Exception
  {
    mockMvc.perform(
      get("/v1/data/unavailable_city"))
           .andExpect(status().is(404));

  }
}