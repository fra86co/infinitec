

To run the tests execute:

mvn clean test

To run the application execute:


mvn spring-boot:run

or

mvn clean install
java -jar target/weather-1.0-SNAPSHOT.jar




I'm started defining an api response (forecast_response.json)
in order to do define and test the rest endpoint using a stubbed response.

Then, I defined main use case RetrieveAverageForecastUseCase in order to model domain classes.

In order to decoupling business logic from supplier I have implemented an adapter
for ForecastRepository in a separate module/package with all the dependecies related to OpenWeather API.

I preferred to use ZonedDateTime of java.time to represent date and time in the business logic
in order to manage correctly time zones (by now always UTC).

I did not make it in time to separate classes represents domain model from
classes that represents rest response for AverageForecastAPI for decoupling
business model from json representation.

